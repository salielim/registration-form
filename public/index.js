(function () {
    var RegApp = angular.module("RegApp", []);

    var RegCtrl = function ($http) {
        var regCtrl = this;

        regCtrl.name = "";
        regCtrl.email = "";
        regCtrl.msg = "";

        regCtrl.submit = function () {
            // console.log("Name: %s, Email: %s", regCtrl.name, regCtrl.email);

            $http.get("/reg", {
                params: {
                    name: regCtrl.name,
                    email: regCtrl.email
                }
            }).then(function(result) { // 200 response
                regCtrl.msg = "Your registration ID is #" + result.data.regid;
            }).catch(function() { // 400, 500 response
                regCtrl.msg = regCtrl.email + " has been registered";
            })
        }

        regCtrl.isValid = function (regForm, fieldName) {
            return (regForm[fieldName].$invalid && regForm[fieldName].$dirty);
        }
    };
    RegCtrl.$inject = ["$http"];

    RegApp.controller("RegCtrl", RegCtrl);
})();